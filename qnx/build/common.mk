ifndef QCONFIG
QCONFIG=qconfig.mk
endif
include $(QCONFIG)

NAME=pandas

DIST_BASE=$(PRODUCT_ROOT)/../

INSTALL_ROOT ?= $(INSTALL_ROOT_$(OS))

ALL_DEPENDENCIES=pandas_all
.PHONY: pandas_all install check clean

include $(MKFILES_ROOT)/qtargets.mk


PANDAS_ROOT = $(PROJECT_ROOT)/../../

PANDAS_VERSION = 2.2.1

BUILD_FLAGS =  --build-temp=$(PROJECT_ROOT)/$(NTO_DIR_NAME)/tmp \
               --build-lib=$(PROJECT_ROOT)/$(NTO_DIR_NAME)/lib \

BUILD_EXT_FLAGS = -I"$(QNX_TARGET)/usr/include:$(QNX_TARGET)/usr/include/$(PYTHON):$(QNX_TARGET)/$(CPUVARDIR)/usr/include:$(QNX_TARGET)/$(CPUVARDIR)/usr/include/$(PYTHON):$(QNX_TARGET)/usr/include/$(CPUVARDIR)/$(PYTHON)" \
                  -L"$(QNX_TARGET)/$(CPUVARDIR)/lib:$(QNX_TARGET)/$(CPUVARDIR)/usr/lib" \
                  -lc++ \
                  -b"$(PROJECT_ROOT)/$(NTO_DIR_NAME)/lib" \


ifndef NO_TARGET_OVERRIDE
pandas_all:
	cd $(PANDAS_ROOT) && \
	rm -rf build && \
	# $(EXPORT_PY) && \
	python3 setup.py build_ext build 

	# python3 setup.py build_ext $(BUILD_EXT_FLAGS) build $(BUILD_FLAGS) dist_info

install check: pandas_all
	@echo Installing!...
	# @cd build && make VERBOSE=1 install $(MAKE_ARGS)
	@echo Done.

clean iclean spotless:
	rm -rf build

uninstall:
endif