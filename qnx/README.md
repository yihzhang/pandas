# Install python3.11
sudo apt-get install -y python3.11-dev python3.11-venv python3.11-distutils

# Submodules
cd pandas
git submodule update --init --recursive

# Create a python virtual environment and install necessary packages
python3 -m venv ~/virtualenvs/pandas-dev
. ~/virtualenvs/pandas-dev/bin/activate
python -m pip install -r requirements-dev.txt

# Build and install for QNX
source <SDP800_path>/qnxsdp-env.sh
JLEVEL=4 make -C qnx/build install
